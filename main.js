"use strict";
// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript

// 1. Прототипное наследование - это механизм js с помощью которого мы можем использовать некоторые свойства, методы и т.д. повторно, при этом не дублировать код и не переписывать его каждый раз в ручную.
// Наследование позволяет видеть и использовать повторно те свойства, которые были переданы от обьекта к обьекту

// Для чого потрібно викликати super() у конструкторі класу-нащадка?
// 2. Вызывается super()  для того что бы наследовать  свойства и/или методы  родительского класса, если мы хотим использовать эти данные снова в предке.
class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }

  set name(value) {
    if (value.length < 3) {
      console.log("Имя слишком короткое");
      return;
    }
    this._name = value;
  }

  get age() {
    return this._age;
  }

  set age(value) {
    if (value < 15) {
      console.log("Вы слишком молоды.");
      return;
    }
    this._age = value;
  }

  get salary() {
    return `The salary of ${this._name} is ${this._salary}`;
  }

  set salary(value) {
    if (typeof value !== "number") {
      console.log("please enter the amount of salary in numbers");
      return;
    }
    this._salary = value;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.languages = lang;
  }
  get salary() {
    return this._salary * 3;
  }
  set salary(value) {
    if (typeof value !== "number") {
      console.log("please enter the amount of salary in numbers");
      return;
    }
    this._salary = value;
  }
}

let firstEmployee = new Employee("Kate", 16, 20000);
let secondEmployee = new Employee("Herantiy", 45, 4000);

let firstProgrammer = new Programmer("Mike", 55, 5000, "ukr, jap, fr");
let secondProgrammer = new Programmer("Helena", 30, 300000, "ukr, ger, eng");
let thirdProgrammer = new Programmer("Alisa", 26, 20000, "ukr, pol, сzech");

console.log(firstEmployee);
console.log(secondEmployee);
console.log(firstProgrammer);
console.log(firstProgrammer.salary);
console.log(secondProgrammer);
console.log(thirdProgrammer);
